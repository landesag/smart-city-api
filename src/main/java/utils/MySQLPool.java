package utils;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp.BasicDataSource;

public final class MySQLPool {

    private static final BasicDataSource DATA_SOURCE = new BasicDataSource();

    private static final String HOST = "localhost";
    private static final String USERNAME = "root";
    private static final String DATABASE = "smart_city_db";
    private static final String PASSWORD = "";

    private MySQLPool() {

    }

    static {
        DATA_SOURCE.setDriverClassName("com.mysql.jdbc.Driver");
        DATA_SOURCE.setUrl("jdbc:mysql://" + HOST + ":3306/" + DATABASE);
        DATA_SOURCE.setUsername(USERNAME);
        DATA_SOURCE.setPassword(PASSWORD);
//        DATA_SOURCE.setInitialSize(1000);
//        DATA_SOURCE.setMaxActive(1000);
//        DATA_SOURCE.setMaxIdle(1000);
    }

    public static Connection getConnection(Integer isolation) throws SQLException {
        Connection con = DATA_SOURCE.getConnection();
        con.setTransactionIsolation(isolation);
        con.setAutoCommit(true);
        return con;
    }

}
