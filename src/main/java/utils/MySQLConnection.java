package utils;

/**
 *
 * @author brunneis
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MySQLConnection {

    private final Properties properties;

    private final String host = "localhost";
    private final String user = "root";
    private final String dbName = "smart_city_db";
    private final String password = "";

    private String getUrl(String host) {
        return "jdbc:mysql://" + host + ":3306/" + dbName;
    }

    public MySQLConnection() {
        properties = new Properties();
        properties.put("user", user);
        properties.put("password", password);
        properties.put("autoReconnect", "true");
        properties.put("characterEncoding", "UTF-8");
        properties.put("useUnicode", "true");
        properties.put("connectionCollation", "utf8mb4_unicode_ci");
        properties.put("characterSetResults", "utf8");
    }

    public Connection getConnection() {
        Connection connection;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
            Logger.getLogger(MySQLConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            connection = DriverManager.getConnection(getUrl(host), properties);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return connection;
    }
}
