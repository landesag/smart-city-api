package vo.response;

/**
 *
 * @author brunneis
 */
public class ResponseContainer {

    private Object data;

    public ResponseContainer(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
