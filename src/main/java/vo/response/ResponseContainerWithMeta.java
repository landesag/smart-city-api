package vo.response;

/**
 *
 * @author brunneis
 */
public class ResponseContainerWithMeta {

    private Object data;
    private Object meta;

    public ResponseContainerWithMeta(Object data, Object meta) {
        this.data = data;
        this.meta = meta;
    }

    public ResponseContainerWithMeta(Object data) {
        this.data = data;
        this.meta = null;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }
}
