/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo.vos;

/**
 *
 * @author lande
 */
public class Position {
    private Float x;
    private Float z;

    public Position() {
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getZ() {
        return z;
    }

    public void setZ(Float z) {
        this.z = z;
    }

    public Position(Float x, Float z) {
        this.x = x;
        this.z = z;
    }
    
}
