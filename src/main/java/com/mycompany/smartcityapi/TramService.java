/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smartcityapi;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import utils.MySQLPool;
import vo.response.ResponseContainer;
import vo.vos.Position;

/**
 *
 * @author brunneis
 */
@Path("/trams")
public class TramService {

    /**
     * Método para recuperar la posicion de un tram
     *
     * @param id_tram
     * @return
     */
    @GET
    @Path("/{id_tram}")
    @Produces("application/json;charset=utf-8")
    public Response getTramPosition(@PathParam("id_tram") String id_tram) {

        try (Connection con = MySQLPool.getConnection(4)) {

            PreparedStatement ps = con.prepareStatement("SELECT * FROM tram_positions WHERE id_tram = ?");
            ps.setString(1, id_tram);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Position pos = new Position(rs.getFloat("x"), rs.getFloat("z"));
                return Response.ok(pos).build();
            } else {
                return Response.serverError().build();

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }

    /**
     * Método para avisar de un accidente de tren
     *
     * @param id_tram
     * @return
     */
    @GET
    @Path("/problem/{id_tram}")
    @Produces("application/json;charset=utf-8")
    public Response sayTramAccident(@PathParam("id_tram") String id_tram) {

        System.out.println("--------------------------\n"
                + "ACCIDENTE DEL TRANVIA " + id_tram + ", CONSULTAR LAS POSICIONESN EN LA BASE DE DATOS\n"
                + "---------------------------");
        return Response.ok().build();

    }

    /**
     * Método para enviar la posicion de un tram
     *
     * @param id_tram
     * @param position
     * @return
     */
    @POST
    @Path("/{id_tram}")
    @Consumes("application/json;charset=utf-8")
    public Response sendTramPosition(@PathParam("id_tram") String id_tram, Position position) {

        try (Connection con = MySQLPool.getConnection(4)) {

            PreparedStatement ps = con.prepareStatement("UPDATE tram_positions SET "
                    + "x=?, z=? WHERE id_tram=?;");
            ps.setFloat(1, position.getX());
            ps.setFloat(2, position.getZ());
            ps.setString(3, id_tram);

            ps.executeUpdate();

            return Response.ok().build();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }

    /**
     * Método para recuperar la posicion de un tram
     *
     * @param id_tram
     * @param x
     * @param z
     * @return
     */
    @GET
    @Path("/{id_tram}/time")
    @Produces("application/json;charset=utf-8")
    @Consumes("application/json;charset=utf-8")
    public Response getTramTimeToMarquesina(@PathParam("id_tram") String id_tram,
            @QueryParam("x") Float x, @QueryParam("z") Float z) {

        try (Connection con = MySQLPool.getConnection(4)) {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM tram_positions WHERE id_tram = ?");
            ps.setString(1, id_tram);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Position pos = new Position(rs.getFloat("x"), rs.getFloat("z"));

                //ALGORITMO PARA CALCULAR EL TIEMPO ENTRE pos Y los x y z obtenidos
                float cateto1 = (pos.getX() - x);
                float cateto2 = (pos.getZ() - z);
                float finalX = (float) Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);

                return Response.ok(new ResponseContainer(round(finalX, 2))).build();
            } else {

                return Response.serverError().build();

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

}
