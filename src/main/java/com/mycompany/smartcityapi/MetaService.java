/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smartcityapi;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import vo.response.ResponseContainer;

/**
 *
 * @author brunneis
 */
@Path("/meta")
public class MetaService {

    /**
     * Método para recuperar la versión de la aplicación
     *
     * @return
     */
    @GET
    @Path("/version")
    @Produces("application/json;charset=utf-8")
    public Response example() {
        return Response.ok(new ResponseContainer("1.0")).build();
    }
}
