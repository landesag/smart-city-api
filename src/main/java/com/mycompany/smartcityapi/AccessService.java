package com.mycompany.smartcityapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import utils.MySQLPool;

/**
 *
 * @author kstromeiraos
 */
@Path("/security")
public class AccessService {

    /**
     * Método para recuperar los accesos no autorizados a un edificio
     *
     * @param building
     * @return
     */
    @GET
    @Path("/{building}")
    @Produces("application/json;charset=utf-8")
    public Response getBuildingAccesses(@PathParam("building") String building) {
        try (Connection con = MySQLPool.getConnection(4)) {

            PreparedStatement ps = con.prepareStatement("SELECT person FROM unauthorized_access WHERE building = ?");
            ps.setString(1, building);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String person = rs.getString(1);
                return Response.ok(person).build();
            } else {
                return Response.serverError().build();

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }
//
//    /**
//     * Método para recuperar los accesos no autorizados a un edificio
//     *
//     * @param person
//     * @return
//     */
//    @GET
//    @Path("/{person}")
//    @Produces("application/json;charset=utf-8")
//    public Response getBuildingAccessesPerson(@PathParam("person") String person) {
//        try (Connection con = MySQLPool.getConnection(4)) {
//
//            PreparedStatement ps = con.prepareStatement("SELECT building FROM unauthorized_access WHERE person = ?");
//            ps.setString(1, person);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                String building = rs.getString(0);
//                return Response.ok(person).build();
//            } else {
//                return Response.serverError().build();
//
//            }
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return Response.serverError().build();
//        }
//    }

    /**
     * Método para insertar un acceso no autorizado
     *
     * @param building
     * @param person
     * @return
     */
    @GET
    @Path("/problem/{building}")
    @Produces("application/json;charset=utf-8")
    public Response sendBuildingAccess(@PathParam("building") String building, @QueryParam("person") String person) {
        try (Connection con = MySQLPool.getConnection(4)) {

            PreparedStatement ps = con.prepareStatement("INSERT INTO unauthorized_access VALUES (?,?) ON DUPLICATE KEY UPDATE building=?;");
            ps.setString(1, building);
            ps.setString(2, person);
            ps.setString(3, building);

            ps.execute();

            return Response.ok().build();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }
}
