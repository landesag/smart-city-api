/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smartcityapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import utils.MySQLPool;
import vo.vos.Position;

/**
 *
 * @author brunneis
 */
@Path("/bags")
public class BagsService {

    /**
     * Método para recuperar la posicion de una mochila
     *
     * @param id_bag
     * @return
     */
    @GET
    @Path("/{id_bag}")
    @Produces("application/json;charset=utf-8")
    public Response getBagPosition(@PathParam("id_bag") String id_bag) {
        try (Connection con = MySQLPool.getConnection(4)) {

            PreparedStatement ps = con.prepareStatement("SELECT * FROM bag_positions WHERE id_bag = ?");
            ps.setString(1, id_bag);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Position pos = new Position(rs.getFloat("x"), rs.getFloat("z"));
                return Response.ok(pos).build();
            } else {
                return Response.serverError().build();

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }

    /**
     * Método para enviar la posicion de una mochila y tambien su dueño
     *
     * @param id_bag
     * @param dueno
     * @param position
     * @return
     */
    @POST
    @Path("/{id_bag}")
    @Consumes("application/json;charset=utf-8")
    public Response sendBag(@PathParam("id_bag") String id_bag, @QueryParam("dueno") String dueno, Position position) {
        try (Connection con = MySQLPool.getConnection(4)) {

            PreparedStatement ps = con.prepareStatement("INSERT INTO bag_positions VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE owner=?, x=?, z=?;");
            ps.setString(1, id_bag);
            ps.setString(2, dueno);
            ps.setFloat(3, position.getX());
            ps.setFloat(4, position.getZ());
            ps.setString(5, dueno);
            ps.setFloat(6, position.getX());
            ps.setFloat(7, position.getZ());


            ps.execute();

            return Response.ok().build();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return Response.serverError().build();
        }
    }

}
