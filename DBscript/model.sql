CREATE TABLE `smart_city_db`.`tram_positions` ( `id_tram` VARCHAR(255) NOT NULL , `x` FLOAT NULL , `z` FLOAT NULL , PRIMARY KEY (`id_tram`)) ENGINE = InnoDB;
CREATE TABLE `smart_city_db`.`bag_positions` ( `id_bag` VARCHAR(255) NOT NULL , `owner` VARCHAR(255) NOT NULL , `x` FLOAT NULL , `z` FLOAT NULL , PRIMARY KEY (`id_bag`)) ENGINE = InnoDB;
CREATE TABLE `smart_city_db`.`unauthorized_access` ( `building` VARCHAR(255) NOT NULL , `person` VARCHAR(255) NOT NULL ,   PRIMARY KEY (`building`, `person`)) ENGINE = InnoDB;
